package handlers

import (
	"html/template"
	"net/http"

	"github.com/gorilla/csrf"
	"github.com/nicksnyder/go-i18n/v2/i18n"
	log "github.com/sirupsen/logrus"
)

type IndexHandler struct {
	bundle *i18n.Bundle
}

func NewIndexHandler(bundle *i18n.Bundle) *IndexHandler {
	return &IndexHandler{bundle: bundle}
}

func (i *IndexHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}
	if r.URL.Path != "/" {
		http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
		return
	}

	localizer := i18n.NewLocalizer(i.bundle, r.Header.Get("Accept-Language"))
	csrGenTitle := localizer.MustLocalize(&i18n.LocalizeConfig{DefaultMessage: &i18n.Message{
		ID:    "CSRGenTitle",
		Other: "CSR generation in browser",
	}})
	nameLabel := localizer.MustLocalize(&i18n.LocalizeConfig{DefaultMessage: &i18n.Message{
		ID:    "NameLabel",
		Other: "Your name",
	}})
	nameHelpText := localizer.MustLocalize(&i18n.LocalizeConfig{DefaultMessage: &i18n.Message{
		ID:    "NameHelpText",
		Other: "Please input your name as it should be added to your certificate",
	}})
	passwordLabel := localizer.MustLocalize(&i18n.LocalizeConfig{DefaultMessage: &i18n.Message{
		ID:    "PasswordLabel",
		Other: "Password for your client certificate",
	}})
	rsaKeySizeLegend := localizer.MustLocalize(&i18n.LocalizeConfig{DefaultMessage: &i18n.Message{
		ID:    "RSAKeySizeLabel",
		Other: "RSA Key Size",
	}})
	rsa3072Label := localizer.MustLocalize(&i18n.LocalizeConfig{DefaultMessage: &i18n.Message{
		ID:    "RSA3072Label",
		Other: "3072 Bit",
	}})
	rsa2048Label := localizer.MustLocalize(&i18n.LocalizeConfig{DefaultMessage: &i18n.Message{
		ID:    "RSA2048Label",
		Other: "2048 Bit (not recommended)",
	}})
	rsa4096Label := localizer.MustLocalize(&i18n.LocalizeConfig{DefaultMessage: &i18n.Message{
		ID:    "RSA4096Label",
		Other: "4096 Bit",
	}})
	rsaHelpText := localizer.MustLocalize(&i18n.LocalizeConfig{DefaultMessage: &i18n.Message{
		ID: "RSAHelpText",
		Other: "An RSA key pair will be generated in your browser. Longer key" +
			" sizes provide better security but take longer to generate.",
	}})
	csrButtonLabel := localizer.MustLocalize(&i18n.LocalizeConfig{DefaultMessage: &i18n.Message{
		ID:    "CSRButtonLabel",
		Other: "Generate signing request",
	}})
	statusLoading := localizer.MustLocalize(&i18n.LocalizeConfig{DefaultMessage: &i18n.Message{
		ID:    "StatusLoading",
		Other: "Loading ...",
	}})
	downloadLabel := localizer.MustLocalize(&i18n.LocalizeConfig{DefaultMessage: &i18n.Message{
		ID:    "DownloadLabel",
		Other: "Download",
	}})
	downloadDescription := localizer.MustLocalize(&i18n.LocalizeConfig{DefaultMessage: &i18n.Message{
		ID: "DownloadDescription",
		Other: "Your key material is ready for download. The downloadable file contains your private key and your" +
			" certificate encrypted with your password. You can now use the file to install your certificate in your" +
			" browser or other applications.",
	}})

	t := template.Must(template.ParseFiles("templates/index.html"))
	err := t.Execute(w, map[string]interface{}{
		"Title":               csrGenTitle,
		"NameLabel":           nameLabel,
		"NameHelpText":        nameHelpText,
		"PasswordLabel":       passwordLabel,
		"RSAKeySizeLegend":    rsaKeySizeLegend,
		"RSA3072Label":        rsa3072Label,
		"RSA2048Label":        rsa2048Label,
		"RSA4096Label":        rsa4096Label,
		"RSAHelpText":         rsaHelpText,
		"CSRButtonLabel":      csrButtonLabel,
		"StatusLoading":       statusLoading,
		"DownloadDescription": downloadDescription,
		"DownloadLabel":       downloadLabel,
		csrf.TemplateTag:      csrf.TemplateField(r),
	})
	if err != nil {
		log.Panic(err)
	}
}
