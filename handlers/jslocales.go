package handlers

import (
	"encoding/json"
	"net/http"
	"strings"

	"github.com/nicksnyder/go-i18n/v2/i18n"
)

type JSLocalesHandler struct {
	bundle *i18n.Bundle
}

func NewJSLocalesHandler(bundle *i18n.Bundle) *JSLocalesHandler {
	return &JSLocalesHandler{bundle: bundle}
}

func (j *JSLocalesHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	parts := strings.Split(r.URL.Path, "/")
	if len(parts) != 4 {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}
	lang := parts[2]

	localizer := i18n.NewLocalizer(j.bundle, lang)

	type translationData struct {
		Keygen struct {
			Started   string `json:"started"`
			Running   string `json:"running"`
			Generated string `json:"generated"`
		} `json:"keygen"`
		Certificate struct {
			Waiting  string `json:"waiting"`
			Received string `json:"received"`
		} `json:"certificate"`
	}

	translations := &translationData{}
	translations.Keygen.Started = localizer.MustLocalize(&i18n.LocalizeConfig{DefaultMessage: &i18n.Message{
		ID:    "JavaScript.KeyGen.Started",
		Other: "started key generation",
	}})
	translations.Keygen.Running = localizer.MustLocalize(&i18n.LocalizeConfig{DefaultMessage: &i18n.Message{
		ID:    "JavaScript.KeyGen.Running",
		Other: "key generation running for __seconds__ seconds",
	}})
	translations.Keygen.Generated = localizer.MustLocalize(&i18n.LocalizeConfig{DefaultMessage: &i18n.Message{
		ID:    "JavaScript.KeyGen.Generated",
		Other: "key generated in __seconds__ seconds",
	}})
	translations.Certificate.Waiting = localizer.MustLocalize(&i18n.LocalizeConfig{DefaultMessage: &i18n.Message{
		ID:    "JavaScript.Certificate.Waiting",
		Other: "waiting for certificate ...",
	}})
	translations.Certificate.Received = localizer.MustLocalize(&i18n.LocalizeConfig{DefaultMessage: &i18n.Message{
		ID:    "JavaScript.Certificate.Received",
		Other: "received certificate from CA",
	}})

	encoder := json.NewEncoder(w)
	if err := encoder.Encode(translations); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
