package handlers

import (
	"encoding/json"
	"net/http"

	log "github.com/sirupsen/logrus"
)

type CertificateSigningHandler struct {
	requestRegistry *SigningRequestRegistry
}

func NewCertificateSigningHandler(requestRegistry *SigningRequestRegistry) *CertificateSigningHandler {
	return &CertificateSigningHandler{requestRegistry: requestRegistry}
}

func (h *CertificateSigningHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, "Only POST requests support", http.StatusMethodNotAllowed)
		return
	}
	if r.Header.Get("content-type") != "application/json" {
		http.Error(w, "Only JSON content is accepted", http.StatusNotAcceptable)
		return
	}
	var err error
	var requestBody requestData

	if err = json.NewDecoder(r.Body).Decode(&requestBody); err != nil {
		log.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	type acceptedResponse struct {
		RequestId string `json:"request_id"`
	}

	taskUuid, err := h.requestRegistry.AddSigningRequest(&requestBody)
	if err != nil {
		log.Error(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusAccepted)
	response := &acceptedResponse{RequestId: taskUuid}
	if err = json.NewEncoder(w).Encode(response); err != nil {
		log.Print(err)
	}
}

type requestData struct {
	Csr        string `json:"csr"`
	CommonName string `json:"common_name"`
}

type responseData struct {
	Certificate string   `json:"certificate"`
	CAChain     []string `json:"ca_chain"`
}
