-- +goose Up

-- LibreSSL - CAcert web application
-- Copyright (C) 2004-2020  CAcert Inc.
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; version 2 of the License.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; if not, write to the Free Software
-- Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

-- changes from version4.sh of the original code base

-- update table admin log

UPDATE `adminlog`
SET `type`        = 'old name or dob change',
	`information` = 'see adminlog_table_backup_1135';

-- alter table admin log

ALTER TABLE `adminlog`
	DROP `old-lname`,
	DROP `old-dob`,
	DROP `new-lname`,
	DROP `new-dob`;

-- Update schema version number
INSERT INTO `schema_version`
	(`version`, `when`)
VALUES ('4', NOW());
