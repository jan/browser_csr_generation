-- +goose Up

-- LibreSSL - CAcert web application
-- Copyright (C) 2004-2020  CAcert Inc.
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; version 2 of the License.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; if not, write to the Free Software
-- Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

-- initial data set from test database

-- tables for cats (https://cats.cacert.org/)
INSERT INTO cats_type (id, type_text)
VALUES (1, 'Assurer Challenge'),
	   (2, 'Org Assurer Test'),
	   (3, 'Triage Challenge'),
	   (5, 'Data Privacy Quiz');

INSERT INTO cats_variant (id, type_id, test_text)
VALUES (5, 1, 'Assurer\'s challenge (EN)'),
	   (6, 1, 'CAcert Assurer Prüfung (DE)'),
	   (4, 1, 'CATS V0.1'),
	   (12, 5, 'Data Privacy Quiz (Generic)'),
	   (15, 5, 'Data Privacy Quiz (Infrastructure Admins)'),
	   (13, 5, 'Data Privacy Quiz (Software)'),
	   (14, 5, 'Data Privacy Quiz (Triage and Support)'),
	   (11, 1, 'Výzva zaru&#269;ovatele (CZ)');


-- country list
-- TODO: fetch from external source like https://github.com/datasets/country-codes
-- TODO: check for completeness / add mapping to ISO country codes?


-- language selection
-- TODO: fetch from external source like https://github.com/datasets/language-codes
--       in combination with http://www.unicode.org/Public/cldr/latest/core.zip
-- TODO: transform to UTF-8, check against current ISO standards


-- region names
-- TODO: take from https://github.com/datasets/fips-10-4

-- locations list is just too big to include here (>110 MiB dump)
-- TODO: take from an external source like https://github.com/datasets/un-locode

;

-- +goose Down
TRUNCATE TABLE cats_variant;
TRUNCATE TABLE cats_type;