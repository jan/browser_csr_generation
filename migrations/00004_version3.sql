-- +goose Up

-- LibreSSL - CAcert web application
-- Copyright (C) 2004-2020  CAcert Inc.
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; version 2 of the License.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; if not, write to the Free Software
-- Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

-- changes from version3.sh of the original code base

-- alter table Admin log
ALTER TABLE `adminlog`
	ADD `type`        varchar(50) NOT NULL,
	ADD `information` varchar(50) NOT NULL;


-- create new table OrgAdminLog
CREATE TABLE IF NOT EXISTS `orgadminlog` (
	`when`        datetime    NOT NULL,
	`oid`         int(11)     NOT NULL,
	`adminid`     int(11)     NOT NULL,
	`type`        varchar(50) NOT NULL,
	`information` varchar(50) NOT NULL
) ENGINE = MyISAM
  DEFAULT CHARSET = latin1;


-- alter table OrgDomainCerts
ALTER TABLE `orgdomaincerts`
	ADD orgadminid       int(11) NULL,
	ADD revokeorgadminid int(11) NULL;


-- alter table OrgEmailCerts
ALTER TABLE `orgemailcerts`
	ADD orgadminid       int(11) NULL,
	ADD revokeorgadminid int(11) NULL;

-- Update schema version number
INSERT INTO `schema_version`
	(`version`, `when`)
VALUES ('3', NOW());
