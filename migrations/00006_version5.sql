-- +goose Up

-- LibreSSL - CAcert web application
-- Copyright (C) 2004-2020  CAcert Inc.
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; version 2 of the License.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; if not, write to the Free Software
-- Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

-- changes from version5.sh of the original code base

-- Move myISAM to InnoDB bug #1172

ALTER TABLE `abusereports`
	ENGINE =INNODB;

ALTER TABLE `addlang`
	ENGINE =INNODB;

ALTER TABLE `adminlog`
	ENGINE =INNODB;

ALTER TABLE `advertising`
	ENGINE =INNODB;

ALTER TABLE `alerts`
	ENGINE =INNODB;

ALTER TABLE `baddomains`
	ENGINE =INNODB;

ALTER TABLE `cats_passed`
	ENGINE =INNODB;

ALTER TABLE `cats_type`
	ENGINE =INNODB;

ALTER TABLE `cats_variant`
	ENGINE =INNODB;

ALTER TABLE `countries`
	ENGINE =INNODB;

ALTER TABLE `disputedomain`
	ENGINE =INNODB;

ALTER TABLE `disputeemail`
	ENGINE =INNODB;

ALTER TABLE `domaincerts`
	ENGINE =INNODB;

ALTER TABLE `domains`
	ENGINE =INNODB;

ALTER TABLE `domlink`
	ENGINE =INNODB;

ALTER TABLE `email`
	ENGINE =INNODB;

ALTER TABLE `emailcerts`
	ENGINE =INNODB;

ALTER TABLE `emaillink`
	ENGINE =INNODB;

ALTER TABLE `gpg`
	ENGINE =INNODB;

ALTER TABLE `languages`
	ENGINE =INNODB;

ALTER TABLE `localias`
	ENGINE =INNODB;

ALTER TABLE `locations`
	ENGINE =INNODB;

ALTER TABLE `news`
	ENGINE =INNODB;

ALTER TABLE `notary`
	ENGINE =INNODB;

ALTER TABLE `org`
	ENGINE =INNODB;

ALTER TABLE `orgadminlog`
	ENGINE =INNODB;

ALTER TABLE `orgdomaincerts`
	ENGINE =INNODB;

ALTER TABLE `orgdomains`
	ENGINE =INNODB;

ALTER TABLE `orgdomlink`
	ENGINE =INNODB;

ALTER TABLE `orgemailcerts`
	ENGINE =INNODB;

ALTER TABLE `orgemaillink`
	ENGINE =INNODB;

ALTER TABLE `orginfo`
	ENGINE =INNODB;

ALTER TABLE `otphashes`
	ENGINE =INNODB;

ALTER TABLE `pinglog`
	ENGINE =INNODB;

ALTER TABLE `regions`
	ENGINE =INNODB;

ALTER TABLE `root_certs`
	ENGINE =INNODB;

ALTER TABLE `schema_version`
	ENGINE =INNODB;

ALTER TABLE `stampcache`
	ENGINE =INNODB;

ALTER TABLE `statscache`
	ENGINE =INNODB;

ALTER TABLE `tickets`
	ENGINE =INNODB;

ALTER TABLE `tverify`
	ENGINE =INNODB;

ALTER TABLE `tverify-vote`
	ENGINE =INNODB;

ALTER TABLE `user_agreements`
	ENGINE =INNODB;

ALTER TABLE `userlocations`
	ENGINE =INNODB;

-- Update schema version number
INSERT INTO `schema_version`
	(`version`, `when`)
VALUES ('5', NOW());
