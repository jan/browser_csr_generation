-- +goose Up

-- LibreSSL - CAcert web application
-- Copyright (C) 2004-2020  CAcert Inc.
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; version 2 of the License.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; if not, write to the Free Software
-- Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

-- changes from version2.sh of the original code base

-- Organisation Assurance bug #1118
ALTER TABLE `orgemailcerts`
	ADD `ou` varchar(50) NOT NULL
		DEFAULT '';


-- Bugs #855, #863, #864, #888, #1118
ALTER TABLE `notary`
	-- add "TTP-Assisted" as method for point transfers (for TTP)
	MODIFY `method`
		enum (
			'Face to Face Meeting',
			'Trusted Third Parties',
			'Thawte Points Transfer',
			'Administrative Increase',
			'CT Magazine - Germany',
			'Temporary Increase',
			'Unknown',
			'TOPUP',
			'TTP-Assisted'
			) NOT NULL DEFAULT 'Face to Face Meeting';

-- Update schema version number
INSERT INTO `schema_version`
	(`version`, `when`)
VALUES ('2', NOW());
