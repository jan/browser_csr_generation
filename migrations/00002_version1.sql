-- +goose Up

-- LibreSSL - CAcert web application
-- Copyright (C) 2004-2020  CAcert Inc.
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; version 2 of the License.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; if not, write to the Free Software
-- Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

-- changes from version1.sh of the original code base

-- CCA agreements and such
CREATE TABLE `user_agreements` (
	`id`       int(11) PRIMARY KEY AUTO_INCREMENT,

	-- the user that agrees
	`memid`    int(11)      NOT NULL,

	-- user that is involved in the agreement (e.g. Assurer)
	`secmemid` int(11)      DEFAULT NULL,

	-- what is being agreed to? e.g. CCA
	`document` varchar(50)  DEFAULT NULL,

	-- when did the agreement take place?
	`date`     datetime     DEFAULT NULL,

	-- whether the user actively agreed or if the agreement took place via
	-- an indirect process (e.g. Assurance)
	`active`   int(1)       NOT NULL,

	-- in which process did the agreement take place (e.g. certificate
	-- issuance, account creation, assurance)
	`method`   varchar(100) NOT NULL,

	-- user comment
	`comment`  varchar(100) DEFAULT NULL
) DEFAULT CHARSET = latin1;


-- description for all certs to make identifying a cert easier
ALTER TABLE `domaincerts`
	ADD `description` varchar(100) NOT NULL
		DEFAULT '';
ALTER TABLE `emailcerts`
	ADD `description` varchar(100) NOT NULL
		DEFAULT '';
ALTER TABLE `gpg`
	ADD `description` varchar(100) NOT NULL
		DEFAULT '';
ALTER TABLE `orgdomaincerts`
	ADD `description` varchar(100) NOT NULL
		DEFAULT '';
ALTER TABLE `orgemailcerts`
	ADD `description` varchar(100) NOT NULL
		DEFAULT '';


-- Bugs #855, #863, #864, #888
ALTER TABLE `notary`
	-- allow for marking as deleted instead of really deleting
	ADD `deleted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',

	-- add "TOPUP" as method for point transfers (for TTP)
	MODIFY `method`
		enum (
			'Face to Face Meeting',
			'Trusted Third Parties',
			'Thawte Points Transfer',
			'Administrative Increase',
			'CT Magazine - Germany',
			'Temporary Increase',
			'Unknown',
			'TOPUP'
			) NOT NULL DEFAULT 'Face to Face Meeting';


-- Organisation Assurance
ALTER TABLE `orginfo`
	-- which Organisation Assurer entered the organisation?
	ADD `creator_id` int(11)  NOT NULL DEFAULT '0',

	-- when was the organisation entered?
	ADD `created`    datetime NOT NULL DEFAULT '0000-00-00 00:00:00',

	-- allow for marking as deleted instead of really deleting
	ADD `deleted`    datetime NOT NULL DEFAULT '0000-00-00 00:00:00';


ALTER TABLE `org`
	-- which Organisation Assurer assigned the Organisation Admin?
	ADD `creator_id` int(11)  NOT NULL DEFAULT '0',

	-- when was the Organisation Admin assigned?
	ADD `created`    datetime NOT NULL DEFAULT '0000-00-00 00:00:00',

	-- allow for marking as deleted instead of really deleting
	ADD `deleted`    datetime NOT NULL DEFAULT '0000-00-00 00:00:00';


-- Update schema version number
INSERT INTO `schema_version`
	(`version`, `when`)
VALUES ('1', NOW());