-- +goose Up

-- LibreSSL - CAcert web application
-- Copyright (C) 2004-2011  CAcert Inc.
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; version 2 of the License.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; if not, write to the Free Software
-- Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

-- changes from version6.sh of the original code base
ALTER TABLE `users`
	ADD `lastLoginAttempt` datetime NULL;

-- Update schema version number
INSERT INTO `schema_version`
	(`version`, `when`)
VALUES ('6', NOW());
