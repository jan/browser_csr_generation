package migrations

import (
	"database/sql"
	"flag"
	"os"

	"github.com/pressly/goose"
	log "github.com/sirupsen/logrus"

	_ "github.com/go-sql-driver/mysql"
)

var (
	flags = flag.NewFlagSet("goose", flag.ExitOnError)
	dir   = flags.String("dir", ".", "directory with migration files")
)

func init() {
	_ = flags.Parse(os.Args[1:])
	args := flags.Args()

	if len(args) < 1 {
		flags.Usage()
		return
	}

	command := args[0]

	var dbUrl string
	var exists bool
	if dbUrl, exists = os.LookupEnv("DB_URL"); !exists {
		log.Fatalf("define database URL in environment variable DB_URL")
	}
	db, err := sql.Open("mysql", dbUrl)
	if err != nil {
		log.Fatalf("goose: failt to open DB: %v\n", err)
	}
	if err = goose.SetDialect("mysql"); err != nil {
		log.Fatalf("failed to set dialect: %v\n", err)
	}

	defer func() {
		if err := db.Close(); err != nil {
			log.Fatalf("goose: failed to close DB: %v\n", err)
		}
	}()

	arguments := make([]string, 0)
	if len(args) > 3 {
		arguments = append(arguments, args[3:]...)
	}

	if err := goose.Run(command, db, *dir, arguments...); err != nil {
		log.Fatalf("goose %v: %v", command, err)
	}
}
