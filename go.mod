module git.dittberner.info/jan/browser_csr_generation

go 1.13

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gobwas/httphead v0.1.0 // indirect
	github.com/gobwas/pool v0.2.1 // indirect
	github.com/gobwas/ws v1.0.4
	github.com/gorilla/csrf v1.7.0
	github.com/nicksnyder/go-i18n/v2 v2.1.1
	github.com/pressly/goose v2.6.0+incompatible
	github.com/sirupsen/logrus v1.7.0
	golang.org/x/text v0.3.4
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
