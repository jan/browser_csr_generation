const {series, parallel, src, dest, watch} = require('gulp');
const csso = require('gulp-csso');
const del = require('delete');
const rename = require('gulp-rename');
const replace = require('gulp-replace');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');

sass.compiler = require('node-sass');

function clean(cb) {
    del(['./public/js/*.js', './public/css/*.css'], cb);
}

function cssTranspile() {
    return src('src/scss/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(dest('public/css'))
        .pipe(sourcemaps.write());
}

function cssMinify() {
    return src('public/css/styles.css')
        .pipe(csso())
        .pipe(rename({extname: '.min.css'}))
        .pipe(dest('public/css'));
}

function jsMinify() {
    return src('src/js/*.js')
        .pipe(uglify())
        .pipe(rename({extname: '.min.js'}))
        .pipe(dest('public/js'));
}

function publishAssets() {
    return src([
        'node_modules/popper.js/dist/*.js',
        'node_modules/popper.js/dist/*.map',
        'node_modules/jquery/dist/*.*',
        'node_modules/bootstrap/dist/js/*.*',
        'node_modules/node-forge/dist/*.*',
        'node_modules/i18next-client/i18next.min.js',
    ]).pipe(dest('public/js'));
}

function publish() {
    return src('src/*.html').pipe(replace('../public/', '')).pipe(dest('public'));
}

exports.default = series(
    clean,
    cssTranspile,
    parallel(cssMinify, jsMinify),
    publishAssets,
    publish
);

exports.watch = function () {
    watch('src/js/*.js', series(jsMinify, publish));
    watch('src/scss/*.scss', series(cssTranspile, cssMinify, publish));
    watch('src/*.html', publish);
}
