# Browser PKCS#10 CSR generation PoC

**Work that started in this repository will continue at
[code.cacert.org](https://code.cacert.org/jandd/poc-browser-csr-generation).**

This repository contains a small proof of concept implementation of browser
based PKCS#10 certificate signing request and PKCS#12 key store generation
using [node-forge](https://github.com/digitalbazaar/forge).

The backend is implemented in [Go](https://golang.org/) and utilizes openssl
for the signing operations. The instructions below have been tested on Debian
11 (Bullseye). Debian 10 works when you use a manual installation of Go.

## Running

1. Install dependencies

   ```
   sudo apt install git npm openssl golang-go
   ```

2. Clone the repository

    ```
    git clone https://git.dittberner.info/jan/browser_csr_generation.git
    ```

3. Get dependencies and build assets

    ```
    cd browser_csr_generation
    npm install --user gulp-cli
    npm install
    ./node_modules/.bin/gulp
    ```

3. Setup the example CA and a server certificate and key

    ```
    ./setup_example_ca.sh
    openssl req -new -x509 -days 365 -subj "/CN=localhost" \
      -addext subjectAltName=DNS:localhost -newkey rsa:3072 \
      -nodes -out server.crt.pem -keyout server.key.pem
    ```

4. Run the Go based backend

    ```
    go run main.go
    ```

   Open https://localhost:8000/ in your browser.

5. Run gulp watch

   You can run a
   [gulp watch](https://gulpjs.com/docs/en/getting-started/watching-files/)
   in a second terminal window to automatically publish changes to the files in
   the `src` directory:

    ```
    gulp watch
    ```

## Translations

This PoC uses [go-i18n](https://github.com/nicksnyder/go-i18n/) for
internationalization (i18n) support.

The translation workflow needs the `go18n` binary which can be installed via

```
go get -u  github.com/nicksnyder/go-i18n/v2/goi18n
```

To extract new messages from the code run

```
goi18n extract
```

Then use

```
goi18n merge active.*.toml
```

to create TOML files for translation as `translate.<locale>.toml`. After
translating the messages run

```
goi18n merge active.*.toml translate.*.toml
```

to merge the messages back into the active translation files. To add a new
language you need to add the language code to `main.go`'s i18n bundle loading
code

```
for _, lang := range []string{"en-US", "de-DE"} {
    if _, err := bundle.LoadMessageFile(fmt.Sprintf("active.%s.toml", lang)); err != nil {
        log.Panic(err)
    }
}
```
