#!/bin/sh

set -eu

COUNTRY_CODE=CH
ORGANIZATION="Acme Ltd."

if [ ! -d "example_ca" ]; then
  mkdir -p example_ca/root/newcerts example_ca/sub/newcerts example_ca/email/newcerts
  touch example_ca/root/index.txt example_ca/sub/index.txt example_ca/email/index.txt
  umask 077
  mkdir example_ca/root/private example_ca/sub/private example_ca/email/private
  openssl req -new -x509 \
    -config ca.cnf \
    -keyout example_ca/root/private/ca.key.pem \
    -newkey rsa:3072 \
    -nodes \
    -subj "/CN=Example Root CA/C=${COUNTRY_CODE}/O=${ORGANIZATION}" \
    -utf8 \
    -days 3650 \
    -out example_ca/root/ca.crt.pem
  chmod +r example_ca/root/ca.crt.pem
  openssl req -new \
    -config ca.cnf \
    -keyout example_ca/sub/private/ca.key.pem \
    -newkey rsa:3072 \
    -nodes \
    -subj "/CN=Example Sub CA/C=${COUNTRY_CODE}/O=${ORGANIZATION}" \
    -utf8 \
    -out example_ca/sub/ca.csr.pem
  openssl ca \
    -config ca.cnf \
    -name rootca \
    -in example_ca/sub/ca.csr.pem \
    -extensions ext_sub_ca \
    -out example_ca/sub/ca.crt.pem \
    -rand_serial \
    -batch
  openssl req -new \
    -config ca.cnf \
    -keyout example_ca/email/private/ca.key.pem \
    -newkey rsa:3072 \
    -nodes \
    -subj "/CN=Example Email CA/C=${COUNTRY_CODE}/O=${ORGANIZATION}" \
    -utf8 \
    -out example_ca/email/ca.csr.pem
  openssl ca \
    -config ca.cnf \
    -name rootca \
    -in example_ca/email/ca.csr.pem \
    -extensions ext_email_ca \
    -out example_ca/email/ca.crt.pem \
    -rand_serial \
    -batch
fi